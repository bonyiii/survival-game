import * as Phaser from "phaser"

function UIBaseScene(name) {
  Phaser.Scene.call(this, name)

  this.margin = 8
  this.uiScale = 1.5
  this._tileSize = 32

  Object.defineProperty(this, 'tileSize', {
    get() {
      return this._tileSize * this.uiScale
    }
  })
}
UIBaseScene.prototype = Object.create(Phaser.Scene.prototype)
UIBaseScene.prototype.constructor = UIBaseScene

export default UIBaseScene
