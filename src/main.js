import Phaser from 'phaser'
import PhaserMatterCollisionPlugin from 'phaser-matter-collision-plugin'

import MainScene from './MainScene'
import InventoryScene from './InventoryScene'
import CraftingScene from './CraftingScene'

const config = {
  width: 512,
  height: 512,
  backgroundColor: '#999999',
  type: Phaser.AUTO,
  parent: 'survival',
  scene: [MainScene, InventoryScene, CraftingScene],
  scale: {
    zoom: 2
  },
  physics: {
    default: 'matter',
    matter: {
      debug: true,
      gravity: { y: 0 }
    }
  },
  plugins: {
    scene: [
      {
        plugin: PhaserMatterCollisionPlugin,
        key: 'matterCollision',
        mapping: 'matterCollision'
      }
    ]
  }
}

new Phaser.Game(config)
