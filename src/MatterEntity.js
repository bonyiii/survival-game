'use strict'

import * as Phaser from 'phaser'
import DropItem from './DropItem'

function MatterEntity(data) {
  const { scene, x, y, health, drops, texture, frame, depth, name } = data
  Phaser.Physics.Matter.Sprite.call(this, scene.matter.world, x, y, texture, frame)
  this.scene.add.existing(this)
  this.x += this.width / 2
  this.y -= this.height / 2
  this.depth = depth || 1
  this.name = name
  this.health = health
  this.drops = JSON.parse(drops)
  this._position = new Phaser.Math.Vector2(this.x, this.y)

  if (this.name) {
    this.sound = scene.sound.add(this.name)
  }

  Object.defineProperties(this, {
    position: {
      get() {
        this._position.set(this.x, this.y)
        return this._position
      }
    },
    velocity: {
      get() {
        return this.body.velocity
      }
    },
    dead: {
      get() {
        return this.health <= 0
      }
    }
  })
}
MatterEntity.prototype = Object.create(Phaser.Physics.Matter.Sprite.prototype)
MatterEntity.prototype.constructor = MatterEntity

MatterEntity.prototype.onDeath = function() { }

const dropMap = {
  201: 'fur',
  203: 'wand_fire',
  241: 'meat',
  280: 'fur',
  273: 'stone',
  272: 'wood',
  228: 'berries'
}

MatterEntity.prototype.hit = function() {
  if (this.sound) { this.sound.play() }
  this.health--
  console.log(`Hitting: ${this.name} Health: ${this.health}`)
  if (this.dead) {
    this.onDeath()
    this.drops.forEach(drop => {
      const name = dropMap[drop]
      new DropItem({ scene: this.scene, x: this.x, y: this.y, frame: drop, name })
    })
  }
}

export default MatterEntity
