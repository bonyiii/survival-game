'use strict'

import items from './Items'

function Inventory() {
  this.maxColumns = 8
  this.maxRows = 3
  this.selected = 0
  this.observers = []

  this.items = {
    //    0: { name: 'pickaxe', quantity: 1},
    3: { name: 'stone', quantity: 3 },
    5: { name: 'shovel', quantity: 1 }
  }
  this.addItem({ name: 'pickaxe', quantity: 2 })
  this.addItem({ name: 'wood', quantity: 2 })

  Object.defineProperty(this, 'selectedItem', {
    get() {
      return this.items[this.selected]
    }
  })
}

Inventory.prototype.getItem = function(index) {
  return this.items[index]
}

Inventory.prototype.removeItem = function(itemName) {
  const existingKey = Object.keys(this.items).find(key => this.items[key].name === itemName)
  if (existingKey) {
    this.items[existingKey].quantity--
    if (this.items[existingKey].quantity <= 0 ) {
      delete this.items[existingKey]
    }
  }
  this.broadcast()
}


Inventory.prototype.subscribe = function(fn) {
  this.observers.push(fn)
}

Inventory.prototype.unsubscribe = function(fn) {
  this.observers = this.observers.filter(subscriber => subscriber !== fn)
}

Inventory.prototype.broadcast = function() {
  this.observers.forEach(subscriber => subscriber())
}

Inventory.prototype.addItem = function(item) {
  const existingKey = Object.keys(this.items).find(key => this.items[key].name === item.name)
  if (existingKey) {
    this.items[existingKey].quantity += item.quantity
  } else {
    for (let index = 0; index < this.maxColumns * this.maxRows; index++) {
      const existingItem = this.items[index]
      if (!existingItem) {
        this.items[index] = item
        break
      }
    }
  }
  this.broadcast()
}

/**
   @param {number} startIndex
   @param {number} endindex
 */
Inventory.prototype.moveItem = function(startIndex, endIndex) {
  if (startIndex === endIndex || this.items[endIndex]) { return }
  this.items[endIndex] = this.items[startIndex]
  delete this.items[startIndex]
  this.broadcast()
}

Inventory.prototype.getItemFrame = function(item) {
  return items[item.name].frame
}

Inventory.prototype.getItemQuantity = function(itemName) {
  return Object.values(this.items)
	       .filter(item => item.name === itemName)
	       .map(i => i.quantity)
	       .reduce((acc, val) => {
		 return acc + val
	       }, 0)
}

export default Inventory
