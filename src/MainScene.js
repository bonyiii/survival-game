'use strict'

import Phaser from 'phaser'
import Player from './Player'
import Resource from './Resource'
import Enemy from './Enemy'
import Crafting from './Crafting'

function MainScene() {
  Phaser.Scene.call(this, 'MainScene')
}
MainScene.prototype = Object.create(Phaser.Scene.prototype)
MainScene.prototype.constructor = MainScene

MainScene.prototype.preload = function() {
  Player.preload(this)
  Resource.preload(this)
  Enemy.preload(this)
  this.load.image('tiles', 'assets/map/RPG Nature Tileset.png')
  this.load.tilemapTiledJSON('map', 'assets/map/survival_meh.json')
}

MainScene.prototype.create = function() {
  const map = this.make.tilemap({ key: 'map' })
  this.map = map
  const tileset = map.addTilesetImage('RPG Nature Tileset', 'tiles', 32, 32, 0, 0)
  this.tileset = tileset
  this.enemies = []

  const layer1 = map.createLayer('Tile Layer 1', tileset, 0, 0)
  const layer2 = map.createLayer('Tile Layer 2', tileset, 0, 0)

  layer1.setCollisionByProperty({ collides: true })
  this.matter.world.convertTilemapLayer(layer1)

  this.addResources()
  this.addEnemies()

  this.player = new Player({ scene: this, x: 210, y: 150, texture: 'female', frame: 'townsfolk_f_idle_1', name: 'player' })
  this.add.existing(this.player)
  //  this.player = new Phaser.Physics.Matter.Sprite(this.matter.world)

  this.player.inputKeys = this.input.keyboard.addKeys({
    up: Phaser.Input.Keyboard.KeyCodes.W,
    down: Phaser.Input.Keyboard.KeyCodes.S,
    left: Phaser.Input.Keyboard.KeyCodes.A,
    right: Phaser.Input.Keyboard.KeyCodes.D
  })

  const camera = this.cameras.main
  camera.zoom = 2
  camera.startFollow(this.player)
  // Start following player after a short delay
  camera.setLerp(0.1, 0.1)
  // Don't display gray are outside of playable game region
  camera.setBounds(0, 0, this.game.config.width, this.game.config.height)
  this.scene.launch('InventoryScene', { mainScene: this })
  this.crafting = new Crafting({ mainScene: this })

  this.input.keyboard.on('keydown-C', () => {
    if (this.scene.isActive('CraftingScene')) {
      this.scene.stop('CraftingScene')
    } else {
      this.scene.launch('CraftingScene', { mainScene: this })
    }
  })
}

MainScene.prototype.update = function() {
  this.player.update()
  this.enemies.forEach(enemy => enemy.update())
}

MainScene.prototype.addResources = function() {
  const resources = this.map.getObjectLayer('Resources')

  resources.objects.forEach(resource => {
    const item = new Resource({ scene: this, tileset: this.tileset, resource })
  })
}

MainScene.prototype.addEnemies = function() {
  const enemies = this.map.getObjectLayer('Enemies')

  enemies.objects.forEach(enemy => {
    this.enemies.push(new Enemy({ scene: this, tileset: this.tileset, enemy }))
  })
}

export default MainScene
