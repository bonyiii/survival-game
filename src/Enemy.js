'use strict'

import Phaser from 'phaser'
import MatterEntity from './MatterEntity'

function Enemy(data) {
  const { scene, enemy, tileset } = data
  const drops = enemy.properties.find(p => p.name === 'drops').value
  const health = enemy.properties.find(p => p.name === 'health').value
  MatterEntity.call(this, {
    scene,
    x: enemy.x,
    y: enemy.y,
    texture: 'enemies',
    frame: `${enemy.name}_idle_1`,
    health,
    name: enemy.name,
    drops
  })

  const { Body, Bodies } = Phaser.Physics.Matter.Matter
  const enemyCollider = Bodies.circle(this.x, this.y, 12, { isSensoer: false, label: 'playerCollider' })
  const enemySensor = Bodies.circle(this.x, this.y, 80, { isSensor: true, lable: 'playerSensor' })

  const compoundBody = Body.create({
    parts: [enemySensor, enemyCollider],
    frictionAir: 0.35
  })

  this.setExistingBody(compoundBody)
  this.setFixedRotation()
  this.scene.matterCollision.addOnCollideStart({
    objectA: [enemySensor],
    callback: other => {
      if (other.gameObjectB && other.gameObjectB.name === 'player') {
        this.attacking = other.gameObjectB
      }
    },
    context: this.scene
  })
}
Enemy.prototype = Object.create(MatterEntity.prototype)
Enemy.prototype.constructor = Enemy

Enemy.preload = function(scene) {
  scene.load.atlas('enemies', 'assets/images/enemies.png', 'assets/images/enemies_atlas.json')
  scene.load.animation('enemies_anim', 'assets/images/enemies_anim.json')
  scene.load.audio('bear', 'assets/audio/bear.mp3')
  scene.load.audio('ent', 'assets/audio/ent.mp3')
  scene.load.audio('wolf', 'assets/audio/wolf.mp3')
}

Enemy.prototype.attack = function(target) {
  if (target.dead || this.dead) {
    clearInterval(this.attackTimer)
    return
  }
  target.hit()
}

Enemy.prototype.update = function() {
  if (this.dead) { return }
  if (this.attacking) {
    const player = this.attacking.position
    const direction = player.subtract(this.position)
    if (direction.length() > 24) {
      const v = direction.normalize()
      this.setVelocityX(direction.x)
      this.setVelocityY(direction.y)
      if (this.attackTimer) {
        clearInterval(this.attackTimer)
        this.attackTimer = null
      }
    } else {
      if (!this.attackTimer) {
        this.attackTimer = setInterval(this.attack, 500, this.attacking)
      }
    }
  }
  this.setFlipX(this.velocity.x < 0)

  if (Math.abs(this.body.velocity.x) > 0.1 || Math.abs(this.body.velocity.y) > 0.1) {
    this.anims.play(`${this.name}_walk`, true)
  } else {
    this.anims.play(`${this.name}_idle`, true)
  }
}

export default Enemy
