'use strict'

import UIBaseScene from './UIBaseScene'

function CraftingScene() {
  UIBaseScene.call(this, 'CraftingScene')

  this.uiScale = 1
  this.craftingSlots = []
}
CraftingScene.prototype = Object.create(UIBaseScene.prototype)
CraftingScene.prototype.constructor = CraftingScene

CraftingScene.prototype.init = function(data) {
  const { mainScene } = data
  this.mainScene = mainScene
  this.crafting = mainScene.crafting
  this.crafting.inventory.subscribe(() => {this.updateCraftableSlots()})
}

CraftingScene.prototype.create = function() {
  this.updateCraftableSlots()

  this.input.on('wheel', (pointer, gameObjects, deltaX, deltaY, deltaZ) => {
    this.crafting.selected = Math.max(0, this.crafting.selected + (deltaY > 0 ? 1 : -1)) % this.crafting.items.length
    this.updateSelected()
  })

  this.input.keyboard.on('keydown-E', () => {
    this.crafting.craft()
  })
}

CraftingScene.prototype.updateSelected = function() {
  for (let index = 0; index < this.crafting.items.length; index++) {
    this.craftingSlots[index].tint = this.crafting.selected === index ? 0xffff00 : 0xffffff
  }
}

CraftingScene.prototype.destroySlot = function(craftingSlot) {
  craftingSlot.matItems.forEach(matItem => matItem.destroy())
  craftingSlot.item.destroy()
  craftingSlot.destroy()
}

CraftingScene.prototype.updateCraftableSlots = function() {
  this.crafting.updateItems()
  const scale = 0.75
  for (let index = 0; index < this.crafting.items.length; index++) {
    if (this.craftingSlots[index]) {
      this.destroySlot(this.craftingSlots[index])
    }
    const craftableItem = this.crafting.items[index]
    let x = this.margin + this.tileSize / 2
    let y = index * this.tileSize + this.game.config.height / 2
    this.craftingSlots[index] = this.add.sprite(x, y, 'items', 0)
    this.craftingSlots[index].item = this.add.sprite(x, y, 'items', craftableItem.frame)
    this.craftingSlots[index].item.tint = craftableItem.canCraft ? 0xffffff : 0x555555
    this.craftingSlots[index].matItems = []
    for (let j = 0; j < craftableItem.matDetails.length; j++) {
      const matItem = craftableItem.matDetails[j]
      this.craftingSlots[index].matItems[j] = this.add.sprite((x + this.tileSize + j * this.tileSize * scale), y, 'items', matItem.frame)
      this.craftingSlots[index].matItems[j].setScale(scale)
      this.craftingSlots[index].matItems[j].tint = matItem.available ? 0xffffff : 0x555555
    }
  }
}

export default CraftingScene
