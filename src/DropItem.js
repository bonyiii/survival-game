'use strict'

import Phaser from 'phaser'

function DropItem(data) {
  const { scene, x, y, frame, name } = data
  Phaser.Physics.Matter.Sprite.call(this, scene.matter.world, x, y, 'items', frame)
  this.name = name
  this.scene.add.existing(this)
  const { Bodies } = Phaser.Physics.Matter.Matter
  const circleCollider = Bodies.circle(this.x, this.y, 10, { isSensor: false, label: 'collider' })
  this.setExistingBody(circleCollider)
  this.setFrictionAir(1)
  this.setScale(0.5)
  this.sound = this.scene.sound.add('pickup')
}
DropItem.prototype = Object.create(Phaser.Physics.Matter.Sprite.prototype)
DropItem.prototype.constructor = DropItem

DropItem.prototype.pickup = function() {
  this.destroy()
  this.sound.play()
  return true
}

export default DropItem
