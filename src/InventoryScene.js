'use strict'

import * as Phaser from 'phaser'
import items from './Items'
import UIBaseScene from './UIBaseScene'

function InventoryScene() {
  UIBaseScene.call(this, 'InventoryScene')

  this.rows = 1
  this.gridSpacing = 4
  this.inventorySlots = []
}
InventoryScene.prototype = Object.create(UIBaseScene.prototype)
InventoryScene.prototype.constructor = InventoryScene

InventoryScene.prototype.updateSelected = function() {
  for (let index = 0; index < this.maxColumns; index++) {
    this.inventorySlots[index].tint = this.inventory.selected === index ? 0xffff00 : 0xffffff
  }
}

/**
   @this Phaser.Scene
 */
InventoryScene.prototype.create = function() {
  // Selection
  this.input.on('wheel', (pointer, gameObject, deltaX, deltaY, deltaZ) => {
    if (this.scene.isActive('CraftingScene')) { return }
    this.inventory.selected = Math.max(0, this.inventory.selected + (deltaY > 0 ? 1 : -1)) % this.maxColumns
    this.updateSelected()
  })

  this.input.keyboard.on('keydown-I', () => {
    this.rows = this.rows === 1 ? this.maxRows : 1
    this.refresh()
  })

  // Dragging
  this.input.setTopOnly(false)
  this.input.on('dragstart', (pointer) => {
    this.startIndex = this.hoverIndex
    this.inventorySlots[this.startIndex].quantityText.destroy()
  })
  this.input.on('drag', (pointer, gameObject, dragX, dragY) => {
    gameObject.x = dragX
    gameObject.y = dragY
  })
  this.input.on('dragend', (pointer) => {
    this.inventory.moveItem(this.startIndex, this.hoverIndex)
    this.refresh()
  })
  this.refresh()
}

InventoryScene.prototype.init = function(data) {
  const { mainScene } = data
  this.mainScene = mainScene
  this.inventory = mainScene.player.inventory
  this.maxColumns = this.inventory.maxColumns
  this.maxRows = this.inventory.maxRows
  this.inventory.subscribe(() => this.refresh())
}

InventoryScene.prototype.destroySlots = function (inventorySlot) {
  if (inventorySlot.item) { inventorySlot.item.destroy() }
  if (inventorySlot.quantityText) { inventorySlot.quantityText.destroy() }
  inventorySlot.destroy()
}

/**
   @this Phaser.Scene
 */
InventoryScene.prototype.refresh = function() {
  this.inventorySlots.forEach(slot => this.destroySlots(slot))
  this.inventorySlots = []
  for (let index = 0; index < this.maxColumns * this.rows; index++) {
    const x = this.margin + this.tileSize / 2 + ((index % this.maxColumns) * (this.tileSize + this.gridSpacing))
    const y = this.margin + this.tileSize / 2 + Math.floor(index / this.maxColumns) * (this.tileSize + this.gridSpacing)
    const inventorySlot = this.add.sprite(x, y, 'items', 175)
    inventorySlot.setScale(this.uiScale)
    inventorySlot.depth = -1
    inventorySlot.setInteractive()
    inventorySlot.on('pointerover', (pointer) => {
      console.log(`pointerver: ${index}`)
      this.hoverIndex = index
    })

    const item = this.inventory.getItem(index)
    if (item) {
      inventorySlot.item = this.add.sprite(inventorySlot.x, inventorySlot.y - this.tileSize / 12, 'items', items[item.name].frame)
      inventorySlot.quantityText = this.add.text(inventorySlot.x, inventorySlot.y + this.tileSize / 6, item.quantity, {
        font: '11px',
        fill: '#111'
      }).setOrigin(0.5, 0)
      // Dragging
      inventorySlot.item.setInteractive()
      this.input.setDraggable(inventorySlot.item)
    }
    this.inventorySlots.push(inventorySlot)
  }
  this.updateSelected()
}

export default InventoryScene
