import Phaser from 'phaser'
import MatterEntity from './MatterEntity'

function Resource(data) {
  const { scene, resource, tileset } = data
  const { type: resourceType } = tileset.getTileData(resource.gid)
  const { yOrigin, drops, depth } = tileset.getTileProperties(resource.gid)
  const name = resourceType.toLowerCase()

  MatterEntity.call(this, {
    scene,
    x: resource.x,
    y: resource.y,
    texture: 'resources',
    frame: name,
    drops,
    depth,
    health: 5,
    name
  })

  this.y = this.y + this.height * (yOrigin - 0.5)
  const { Bodies } = Phaser.Physics.Matter.Matter
  const circleCollider = Bodies.circle(this.x, this.y, 12, { isSensor: false, label: 'collider' })
  this.setExistingBody(circleCollider)
  this.setOrigin(0.5, yOrigin)
  this.setStatic(true)
}
Resource.prototype = Object.create(MatterEntity.prototype)
Resource.prototype.constructor = Resource

Resource.preload = function(scene) {
  scene.load.atlas('resources', 'assets/images/resources.png', 'assets/images/resources_atlas.json')
  scene.load.audio('tree', 'assets/audio/tree.mp3')
  scene.load.audio('rock', 'assets/audio/rock1.wav')
  scene.load.audio('bush', 'assets/audio/bush.wav')
  scene.load.audio('pickup', 'assets/audio/pickup.wav')
}

export default Resource
