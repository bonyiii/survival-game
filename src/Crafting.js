'use strict'

import DropItem from './DropItem'
import items from './Items'

function Crafting(data) {
  const { mainScene } = data
  this.mainScene = mainScene
  this.inventory = mainScene.player.inventory
  this.player = mainScene.player
  this.selected = 0
  this.items = []
}

// Crafting.prototype = Object.create(UIBaseScene.prototype)
// Crafting.prototype.constructor = Crafting

Crafting.prototype.craft = function() {
  let item = this.items[this.selected]
  if ( item.canCraft) {
    new DropItem({ name: item.name,
		   scene: this.mainScene,
		   x: this.player.x - 32,
		   y: this.player.y - 32,
		   frame: item.frame
    })
    item.matDetails.forEach(matDetail => {
      this.inventory.removeItem(matDetail.name)
    })
  }
}

Crafting.prototype.updateItems = function() {
  this.items = []
  const craftables = Object.keys(items).filter(i => items[i].mats)
  for (let i = 0; i < craftables.length; i++) {
    const itemName = craftables[i]
    const mats = items[itemName].mats
    // check if we have enough materials

    let lastMat = ''
    let matDetails = []
    let canCraft = true
    let qty = 0
    mats.forEach(mat => {
      qty = (lastMat === mat) ? qty-1 : this.inventory.getItemQuantity(mat)
      const available = (qty > 0)
      matDetails.push({ name: mat, frame: items[mat].frame, available })
      lastMat = mat
      if (!available) { canCraft = false }
    })
    this.items.push({name: itemName, frame: items[itemName].frame, matDetails, canCraft })
  }
  console.log(this.items)
}

export default Crafting
